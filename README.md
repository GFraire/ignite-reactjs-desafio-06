<p align="center">
  <img alt="worldtrip" src=".github/logo.svg" width="160px">
</p>

<p align="center">

  <!-- <img src="https://img.shields.io/gitlab/stars/GFraire/ignite-reactjs-desagio-06?label=stars&message=MIT&color=8257E5&labelColor=000000" alt="Stars"> -->

  <img  src="https://img.shields.io/static/v1?label=license&message=MIT&color=8257E5&labelColor=000000" alt="License">
</p>

<h1 align="center">
    <img alt="worldtrip" src=".github/cover.png" />
</h1>

<br>

## 🧪 Tecnologias

Esse projeto foi desenvolvido com as seguintes tecnologias:

- [React](https://reactjs.org)
- [Next](https://nextjs.org/)
- [Chakra UI](https://chakra-ui.com/)
- [SwiperJS](https://swiperjs.com/)

## 🚀 Como executar

Clone o projeto e acesse a pasta do mesmo.

```bash
$ git clone https://gitlab.com/GFraire/ignite-reactjs-desafio-06
$ cd ignite-reactjs-desafio-06
```

Para iniciá-lo, siga os passos abaixo:

```bash
# Instalar as dependências
$ yarn
# Iniciar o projeto
$ yarn start
```

O app estará disponível no seu browser pelo endereço http://localhost:3000.

## 💻 Projeto

Worldtrip é um site de viagens para quem está a procura de um novo local para conhecer.

Este é um projeto desenvolvido durante o curso **[Ignite](https://app.rocketseat.com.br/ignite/react-js)** em 2022

## 🔖 Layout

Você pode visualizar o layout do projeto através do link abaixo:

- [Layout Web](<https://www.figma.com/file/kRLEQMLC4a0SZS7inNikw8/Desafio-1-M%C3%B3dulo-4-ReactJS-(Copy)>)

Lembrando que você precisa ter uma conta no [Figma](http://figma.com/).

<!-- ## 📝 License

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes. -->

---
