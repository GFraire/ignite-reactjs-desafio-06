import { ChakraProvider } from "@chakra-ui/react";
import { theme } from "../styles/theme";
import { AppProps } from "next/app";

import "../styles/global.css";
import { Header } from "../components/Header";
import { useRouter } from "next/router";

function MyApp({ Component, pageProps }: AppProps) {
  const { asPath } = useRouter();
  const useIcon = asPath === "/" ? false : true;
  return (
    <ChakraProvider theme={theme}>
      <Header returnIcon={useIcon} />
      <Component {...pageProps} />
    </ChakraProvider>
  );
}

export default MyApp;
