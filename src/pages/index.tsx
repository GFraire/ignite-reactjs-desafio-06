import { Center, Divider } from "@chakra-ui/react";
import { Introduction } from "../components/Introduction";
import { Slider } from "../components/Slider/Slider";

export default function Home() {
  return (
    <>
      <Introduction />
      <Center marginY="10">
        <Divider height="2px" width="90px" bgColor="gray.600" />
      </Center>
      <Slider />
    </>
  );
}
