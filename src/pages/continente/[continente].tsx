import { Box, Flex, Heading, SimpleGrid, Text } from "@chakra-ui/react";
import { continentData } from "../../data/countryData";
import { useRouter } from "next/router";
import { InfoContinent } from "../../components/InfoContinent";
import { CityCard } from "../../components/CityCard";

export default function Country() {
  const { query } = useRouter();
  const { continente } = query;

  return (
    <>
      {continentData
        .filter(
          (continent) =>
            continent.name
              .normalize("NFD")
              .replace(/[\u0300-\u036f]/g, "")
              .toLowerCase() == continente
        )
        .map((continent) => (
          <Box as="main">
            <Box
              w="100%"
              minHeight={500}
              background="black"
              backgroundRepeat="no-repeat"
              backgroundSize="cover"
              maxHeight={500}
              backgroundImage={continent.backgroundImg}
              position="relative"
            >
              <Text
                color="gray.50"
                fontWeight="semibold"
                fontSize={48}
                position="absolute"
                transform={{ base: "translate(50%, 50%)" }}
                right={{ base: "50%", xl: "85%" }}
                bottom={{ base: "50%", xl: "15%" }}
              >
                {continent.name}
              </Text>
            </Box>

            <Box maxWidth={1160} mx="auto" mb={7}>
              <SimpleGrid mt="16" columns={{ base: 1, xl: 2 }} spacing={12}>
                <Text
                  maxWidth={600}
                  mx={{ base: 4, xl: "auto" }}
                  lineHeight={2}
                  textAlign="justify"
                  color="gray.600"
                >
                  {continent.text}
                </Text>

                <Flex gap={10} mx="auto">
                  <InfoContinent number={continent.countrys} text="países" />
                  <InfoContinent number={continent.languages} text="línguas" />
                  <InfoContinent
                    number={continent.cityCount}
                    text="cidades + 100"
                  />
                </Flex>
              </SimpleGrid>

              <Box mt="10">
                <Heading
                  as="h2"
                  size="lg"
                  fontWeight="medium"
                  color="gray.600"
                  ml={{ base: 4, sm: 6, xl: 0 }}
                >
                  Cidades + 100
                </Heading>

                <SimpleGrid
                  mt={8}
                  columns={{ base: 1, sm: 2, md: 3, xl: 4 }}
                  spacing={8}
                >
                  {continent.citys.map((city) => (
                    <CityCard
                      key={city.name}
                      name={city.name}
                      bgImage={city.image}
                      country={city.countryName}
                      countryImg={city.countryImg}
                    />
                  ))}
                </SimpleGrid>
              </Box>
            </Box>
          </Box>
        ))}
    </>
  );
}
