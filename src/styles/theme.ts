import { extendTheme, Theme } from "@chakra-ui/react";

const customTheme = {
  colors: {
    gray: {
      50: "#F5F8FA",
      200: "#DADADA",
      400: "#999999",
      600: "#47585B",
    },
    yellow: {
      500: "#FFBA08",
    },
  },
  fonts: {
    heading: "Poppins",
    body: "Poppins",
  },
  space: {
    22: "140px",
  },
  styles: {
    global: {
      body: {
        bg: "gray.50",
      },
      card: {
        fontFamily: "Barlow",
      },
    },
  },
} as Theme;

export const theme = extendTheme(customTheme);
