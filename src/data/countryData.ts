interface continentDataInfo {
  name: string;
  backgroundImg: string;
  text: string;
  countrys: number;
  languages: number;
  cityCount: number;
  citys: cytyDataInfo[];
}

interface cytyDataInfo {
  name: string;
  image: string;
  countryImg: string;
  countryName: string;
}

export const continentData: continentDataInfo[] = [
  {
    name: "Europa",
    backgroundImg: "/assets/continent/europe.png",
    text: "A Europa é, por convenção, um dos seis continentes do mundo. Compreendendo a península ocidental da Eurásia, a Europa geralmente divide-se da Ásia a leste pela divisória de águas dos montes Urais, o rio Ural, o mar Cáspio, o Cáucaso, e o mar Negro a sudeste",
    countrys: 50,
    languages: 60,
    cityCount: 27,
    citys: [
      {
        name: "Londres",
        image: "/assets/city/europe/londres.png",
        countryName: "Reino Unido",
        countryImg: "/assets/country/europe/reino-unido.png",
      },
      {
        name: "Paris",
        image: "/assets/city/europe/paris.png",
        countryName: "França",
        countryImg: "/assets/country/europe/franca.png",
      },
      {
        name: "Roma",
        image: "/assets/city/europe/roma.png",
        countryName: "Itália",
        countryImg: "/assets/country/europe/italia.png",
      },
      {
        name: "Praga",
        image: "/assets/city/europe/praga.png",
        countryName: "República Tcheca",
        countryImg: "/assets/country/europe/republica-tcheca.png",
      },
      {
        name: "Amsterdã",
        image: "/assets/city/europe/amsterda.png",
        countryName: "Holanda",
        countryImg: "/assets/country/europe/holanda.png",
      },
    ],
  },
  {
    name: "Ásia",
    backgroundImg: "/assets/continent/asia.png",
    text: "A Europa é, por convenção, um dos seis continentes do mundo. Compreendendo a península ocidental da Eurásia, a Europa geralmente divide-se da Ásia a leste pela divisória de águas dos montes Urais, o rio Ural, o mar Cáspio, o Cáucaso, e o mar Negro a sudeste",
    countrys: 50,
    languages: 60,
    cityCount: 109,
    citys: [
      {
        name: "Londres",
        image: "/assets/city/europe/londres.png",
        countryName: "Reino Unido",
        countryImg: "/assets/country/europe/reino-unido.png",
      },
      {
        name: "Paris",
        image: "/assets/city/europe/paris.png",
        countryName: "França",
        countryImg: "/assets/country/europe/franca.png",
      },
      {
        name: "Roma",
        image: "/assets/city/europe/roma.png",
        countryName: "Itália",
        countryImg: "/assets/country/europe/italia.png",
      },
      {
        name: "Praga",
        image: "/assets/city/europe/praga.png",
        countryName: "República Tcheca",
        countryImg: "/assets/country/europe/republica-tcheca.png",
      },
      {
        name: "Amsterdã",
        image: "/assets/city/europe/amsterda.png",
        countryName: "Holanda",
        countryImg: "/assets/country/europe/holanda.png",
      },
    ],
  },
];
