import { Flex, Text } from "@chakra-ui/react";

interface InfoContinentProps {
  number: number;
  text: string;
}

export function InfoContinent({ number, text }: InfoContinentProps) {
  return (
    <Flex direction="column" gap={0} justify="center" align="center">
      <Text
        color="yellow.500"
        lineHeight={0.9}
        fontSize="40"
        fontWeight="semibold"
      >
        {number}
      </Text>
      <Text fontWeight="semibold" color="gray.700" fontSize="20">
        {text}
      </Text>
    </Flex>
  );
}
