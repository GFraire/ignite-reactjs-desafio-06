import { Box, Image, Text } from "@chakra-ui/react";

interface InfoProps {
  title: string;
  iconPath: string;
}

export function Info({ iconPath, title }: InfoProps) {
  return (
    <Box display={"flex"} alignItems={"center"} flexDirection={"column"}>
      <Image src={iconPath} alt={title} />
      <Text
        color={"gray.600"}
        fontWeight={"semibold"}
        fontSize={{ base: "15px", md: "24px" }}
        marginTop={6}
      >
        {title}
      </Text>
    </Box>
  );
}
