import { Box, Flex, Icon, Image } from "@chakra-ui/react";
import Link from "next/link";
import { FiChevronLeft } from "react-icons/fi";

interface HeaderProps {
  returnIcon: boolean;
}

export function Header({ returnIcon }: HeaderProps) {
  return (
    <Flex
      position="relative"
      w="100%"
      justify="center"
      align="center"
      paddingY={"5"}
    >
      {returnIcon && (
        <Link href="/" passHref>
          <Box as="a">
            <Icon
              position="absolute"
              left="10"
              as={FiChevronLeft}
              fontSize={24}
            />
          </Box>
        </Link>
      )}
      <Image src="logo.svg" alt="Logo" height={["30px", "30px", "45px"]} />
    </Flex>
  );
}
