import { Flex, GridItem, Icon, Text } from "@chakra-ui/react";
import { FaCircle } from "react-icons/fa";

interface GridItemInfoProps {
  title: string;
  gridColumnStart?: number;
  gridColumnEnd?: number;
  margin?: string;
}
export function GridItemInfo({
  title,
  gridColumnStart,
  gridColumnEnd,
  margin,
}: GridItemInfoProps) {
  return (
    <GridItem
      gridColumnStart={gridColumnStart}
      gridColumnEnd={gridColumnEnd}
      margin={margin}
    >
      <Flex align="center" gap="8px">
        <Icon as={FaCircle} height="8px" color="yellow.500" />
        <Text
          color={"gray.600"}
          fontWeight={"semibold"}
          fontSize={{ base: "18px" }}
        >
          {title}
        </Text>
      </Flex>
    </GridItem>
  );
}
