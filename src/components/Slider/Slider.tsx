import { Box, Flex, Heading, Text } from "@chakra-ui/react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Keyboard, Pagination, Navigation } from "swiper";
import { dataContinent } from "./data";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import Link from "next/link";

// import required modules

export function Slider() {
  return (
    <Flex direction="column" justify="center" marginTop="12" marginBottom="10">
      <Heading
        textAlign="center"
        color="gray.600"
        fontWeight="medium"
        fontSize={{ base: "20px", md: "36px" }}
      >
        Vamos nessa? <br /> Então escolha seu continente
      </Heading>

      <Box
        marginTop="10"
        maxWidth="1250px"
        maxHeight="450px"
        width="100%"
        height="450px"
        display="flex"
        alignSelf="center"
      >
        <Swiper
          className="swiper"
          slidesPerView={1}
          spaceBetween={30}
          keyboard={{
            enabled: true,
          }}
          pagination={{
            clickable: true,
          }}
          navigation={true}
          modules={[Keyboard, Pagination, Navigation]}
        >
          {dataContinent.map((continent) => (
            <SwiperSlide className="swiper-slide">
              <Flex
                width="100%"
                height="100%"
                backgroundImage={continent.imgPath}
                backgroundRepeat="no-repeat"
                backgroundSize="cover"
                justify="center"
                align="center"
                direction="column"
              >
                <Link href={continent.link}>
                  <a>
                    <Heading
                      color={continent.color}
                      fontWeight="bold"
                      fontSize={{ base: "24px", md: "48px" }}
                    >
                      {continent.name}
                    </Heading>
                    <Text
                      marginTop="4"
                      color={continent.color}
                      fontWeight="bold"
                      fontSize={{ base: "14px", md: "24px" }}
                    >
                      {continent.description}
                    </Text>
                  </a>
                </Link>
              </Flex>
            </SwiperSlide>
          ))}
        </Swiper>
      </Box>
    </Flex>
  );
}
