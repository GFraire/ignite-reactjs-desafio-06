interface dataContinentProperties {
  name: string;
  description: string;
  imgPath: string;
  color: string;
  link: string;
}

export const dataContinent = <dataContinentProperties[]>[
  {
    name: "Europa",
    description: "O continente mais antigo.",
    imgPath: "assets/continent/europe.png",
    color: "gray.50",
    link: "/continente/europa",
  },
  {
    name: "Ásia",
    description: "O maior dos continentes.",
    imgPath: "assets/continent/asia.png",
    color: "gray.50",
    link: "/continente/asia",
  },
  {
    name: "América do Norte",
    description: "Composta por apenas três países.",
    imgPath: "assets/continent/north-america.png",
    color: "gray.50",
    link: "/continente/america-norte",
  },
  {
    name: "América do Sul",
    description:
      "Abrange 12% da superfície terrestre e 6% da população mundial.",
    imgPath: "assets/continent/south-america.png",
    color: "black",
    link: "/continente/america-sul",
  },
  {
    name: "África",
    description: "Terceiro continente mais extenso.",
    imgPath: "assets/continent/africa.jpg",
    color: "black",
    link: "/continente/africa",
  },
  {
    name: "Oceania",
    description: "Composta por vários grupos de ilhas do oceano Pacífico..",
    imgPath: "assets/continent/oceania.jpg",
    color: "gray.50",
    link: "/continente/oceania",
  },
];
