import {
  Box,
  Flex,
  Grid,
  Heading,
  HStack,
  Image,
  Text,
  useBreakpointValue,
  VStack,
} from "@chakra-ui/react";
import { GridItemInfo } from "./GridItemInfo";
import { Info } from "./Info";

export function Introduction() {
  const isDesktop = useBreakpointValue({
    base: false,
    md: true,
  });

  return (
    <VStack>
      <Flex
        backgroundImage="assets/background.png"
        backgroundRepeat="no-repeat"
        backgroundSize="cover"
        justify="space-around"
        align="center"
        width="100%"
        height={{ base: "163px", md: "335px" }}
        marginBottom="14"
      >
        <Box>
          <Heading
            fontSize={{ base: "20px", md: "36px" }}
            fontWeight="500"
            lineHeight="1.5"
            size="lg"
            color="gray.50"
          >
            5 Continentes, <br />
            infinitas possibilidades.
          </Heading>

          <Text color="gray.200" marginTop={{ base: 3, md: 6 }}>
            Chegou a hora de tirar do papel a viagem que você <br />
            sempre sonhou.
          </Text>
        </Box>

        {isDesktop && (
          <Image
            alignSelf={"flex-end"}
            marginBottom={"-10"}
            src="assets/airplane.svg"
            alt="Avião"
          />
        )}
      </Flex>

      {!isDesktop ? (
        <Grid templateColumns="1fr 1fr" columnGap="30px" rowGap="15px">
          <GridItemInfo title="vida noturna" />
          <GridItemInfo title="praia" />
          <GridItemInfo title="moderno" />
          <GridItemInfo title="clássico" />
          <GridItemInfo
            gridColumnStart={1}
            gridColumnEnd={3}
            margin="auto"
            title="e mais..."
          />
        </Grid>
      ) : (
        <HStack spacing={{ base: "14", lg: "24" }} display="flex">
          <Info title="vida noturna" iconPath="assets/cocktail.svg" />
          <Info title="praia" iconPath="assets/surf.svg" />
          <Info title="moderno" iconPath="assets/building.svg" />
          <Info title="clássico" iconPath="assets/museum.svg" />
          <Info title="e mais..." iconPath="assets/earth.svg" />
        </HStack>
      )}
    </VStack>
  );
}
