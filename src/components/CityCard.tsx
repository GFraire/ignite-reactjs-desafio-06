import { Box, Flex, Image, Text } from "@chakra-ui/react";

interface cityCardProps {
  name: string;
  bgImage: string;
  country: string;
  countryImg: string;
}

export function CityCard({
  name,
  bgImage,
  country,
  countryImg,
}: cityCardProps) {
  return (
    <Box
      borderRadius="lg"
      maxWidth={300}
      width="100%"
      mx={{ base: "auto", xl: 0 }}
    >
      <Image src={bgImage} width="100%" maxHeight="70%" />

      <Flex
        justifyContent="space-between"
        align="center"
        borderWidth="1px"
        borderTop="0px"
        borderStyle="solid"
        borderColor="yellow.500"
        borderBottomRadius="lg"
        pt={4}
        pb={6}
        px={6}
      >
        <Box>
          <Text
            fontSize={20}
            fontFamily="Barlow"
            color="gray.600"
            fontWeight="semibold"
          >
            {name}
          </Text>
          <Text
            fontSize={16}
            color="gray.400"
            fontWeight="medium"
            fontFamily="Barlow"
          >
            {country}
          </Text>
        </Box>
        <Image w={30} h={30} src={countryImg} />
      </Flex>
    </Box>
  );
}
